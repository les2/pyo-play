from pyo import *
import time
s = Server().boot()
freq = 100
a = Sine(freq, 0, 1).out()
b = Sine(freq*2, 0, 1).out()
c = Sine(freq/3, 0, 1).out()
d = PinkNoise(.7).mix(2).out()
pauseDelay = .1
toneDuration = 1
maxFreq = 25000
freqIncrement = 1000

s.start()
while freq < maxFreq:
    print("Frequency=%s" % freq)
    time.sleep(toneDuration)
    #s.stop()
    freq += freqIncrement
    a.setFreq(freq)
    if freq*2 < maxFreq:
        b.setFreq(freq*2)
    c.setFreq(freq/3)
    time.sleep(pauseDelay)
s.stop()
